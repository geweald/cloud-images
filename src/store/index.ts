import ImagesStore from './images';
import ApiService from '../services/api';

export class RootStore {
  images: ImagesStore;

  constructor() {
    this.images = new ImagesStore(this, new ApiService());
  }
}

export const store = new RootStore();

export const useStore = () => {
  return store;
};

export const useImagesStore = () => {
  return store.images;
};
