import { Image, ImagesStore } from './images.store';
import { ApiService } from '../../services/api/api.service';
import { RootStore } from '../index';
import { ApiImage } from '../../services/api/api.types';

describe('ImagesStore', () => {
  let store: ImagesStore;
  let apiService: ApiService = new ApiService();

  beforeEach(() => {
    apiService.fetchImages = jest.fn();
    store = new ImagesStore(new RootStore(), apiService);
  });

  it('should load images after creation', function() {
    expect(apiService.fetchImages).toBeCalled();
  });

  describe('loadImages', () => {
    it('should set error on exception', function() {
      apiService.fetchImages = jest.fn().mockImplementation(() => {
        throw new Error('xxx');
      });
      return store.loadImages().then(() => {
        expect(store.error).toEqual('xxx');
      });
    });

    it('should set images properly', function() {
      const mockImg = { bigUrl: 'bigUrl', id: '1', name: 'text', url: 'url' };
      apiService.fetchImages = jest.fn().mockImplementation((): ApiImage[] => {
        return [mockImg];
      });
      return store.loadImages().then(() => {
        expect(store.error).toEqual('');
        expect(store.images).toEqual({ [mockImg.id]: new Image(mockImg) });
        expect(store.loading).toEqual(false);
      });
    });
  });
});
