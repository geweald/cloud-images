import { ApiService } from '../../services/api/api.service';
import { action, computed, flow, observable } from 'mobx';
import { ApiImage } from '../../services/api/api.types';
import { RootStore } from '../index';

export class Image {
  @observable name = '';
  source: ApiImage;

  constructor(image: ApiImage) {
    this.source = image;
    this.name = image.name;
  }
}

export class ImagesStore {
  @observable loading = true;
  @observable error = '';
  @observable images: { [key: string]: Image } = {};

  constructor(private rootStore: RootStore, private api: ApiService) {
    this.loadImages();
  }

  @computed get imagesList() {
    return Object.values(this.images);
  }

  getImageById = (id: string) => {
    return this.images[id];
  };

  loadImages = flow(function*(this: ImagesStore) {
    try {
      this.loading = true;

      const images: any = yield this.api.fetchImages();
      images.forEach((image: ApiImage) => {
        this.images[image.id] = new Image(image);
      });

      this.error = '';
    } catch (e) {
      this.error = e.message;
    } finally {
      this.loading = false;
    }
  });

  @action.bound removeImage = (id: string) => {
    delete this.images[id];
  };

  @action.bound setImageName = (id: string, name: string) => {
    this.images[id].name = name;
  };
}
