import React from 'react';
import { useStyles } from './image-card.styles';
import clsx from 'clsx';

export interface ImageCardProps {
  image: {
    source: string;
    width: number;
    height: number;
  };
  text: string;
  className?: string;
}

export const ImageCard: React.FC<ImageCardProps> = props => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, props.className)}>
      <img src={props.image.source} width={300} height={200} />
      <h2>{props.text}</h2>
    </div>
  );
};
