import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles({
  container: {
    display: 'flex',
    flexDirection: 'column'
  },
  image: {
    width: '300px',
    height: '200px'
  },
  text: {
    fontSize: '14px',
    textAlign: 'center'
  }
});
