import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  imageLink: {
    margin: '10px',
    textDecoration: 'none'
  },
  header: {
    textAlign: 'center'
  }
});
