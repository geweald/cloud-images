import React from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';
import { useImagesStore } from '../../store';
import { useStyles } from './home.styles';
import ImageCard from '../../components/image-card';

export const HomeScreen: React.FC = observer(props => {
  const store = useImagesStore();
  const styles = useStyles();

  return (
    <div>
      <h1 className={styles.header}>Home</h1>
      <div className={styles.container}>
        {store.loading && <p>Loading...</p>}
        {store.error && <p>{store.error}</p>}

        {store.imagesList.map(image => (
          <Link className={styles.imageLink} key={image.source.id} to={`/image-details/${image.source.id}`}>
            <ImageCard image={{ source: image.source.url, width: 300, height: 200 }} text={image.name} />
          </Link>
        ))}
      </div>
    </div>
  );
});
