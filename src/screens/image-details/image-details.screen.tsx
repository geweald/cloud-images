import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { useImagesStore } from '../../store';
import { observer } from 'mobx-react';
import { useStyles } from './image-details.styles';

export interface ImageDetailsScreenProps {}

export const ImageDetailsScreen: React.FC<ImageDetailsScreenProps> = observer(props => {
  const styles = useStyles();
  const { id = '' }: { id?: string } = useParams();
  const store = useImagesStore();
  const image = store.getImageById(id);

  if (!id || !image) {
    return <div>No such image</div>;
  }

  return (
    <div className={styles.container}>
      <img className={styles.image} src={image.source.bigUrl} />
      <input
        className={styles.input}
        value={image.name}
        onChange={e => {
          store.setImageName(id, e.target.value);
        }}
      />
      <Link to={'/home'} className={styles.backLink}>
        {'<- Back to home'}
      </Link>

      <Link
        to={'/'}
        onClick={() => {
          store.removeImage(id);
        }}
      >
        Remove
      </Link>
    </div>
  );
});
