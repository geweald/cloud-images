import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  input: {
    margin: '10px',
    padding: '5px'
  },
  backLink: {
    padding: '10px',
    textDecoration: 'none'
  },
  image: {
    maxWidth: '100%'
  }
});
