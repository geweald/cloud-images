export interface ApiImage {
  id: string;
  name: string;
  url: string;
  bigUrl: string;
}
