import { apiMock } from './api.mock';

export class ApiService {
  fetchImages() {
    return Promise.resolve(apiMock);
  }
}
