import { ApiImage } from './api.types';

export const apiMock = new Array(30).fill(0).map(
  (_, i): ApiImage => {
    return {
      id: i.toString(),
      name: `Image ${i}`,
      url: `https://picsum.photos/id/${i}/300/200`,
      bigUrl: `https://picsum.photos/id/${i}/600/400`
    };
  }
);
