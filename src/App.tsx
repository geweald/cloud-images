import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { ImageDetailsScreen } from './screens/image-details/image-details.screen';
import { HomeScreen } from './screens/home/home.screen';

const App: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={HomeScreen} />
        <Route path="/image-details/:id" exact component={ImageDetailsScreen} />
        <Redirect to="/" />
      </Switch>
    </Router>
  );
};

export default App;
